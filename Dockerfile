FROM nginx:alpine

RUN apk add --no-cache moreutils openssl python3 py3-pip py3-requests py3-beautifulsoup4 \
 && pip install pyjwt

COPY docker/ /
