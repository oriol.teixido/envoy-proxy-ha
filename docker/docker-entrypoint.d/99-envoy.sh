#!/bin/sh
if [ -z "${ENVOY_USERNAME}" ]; then
  echo "ERROR. Empty ENVOY_USERNAME variable."
  exit 1
fi

if [ -z "${ENVOY_PASSWORD}" ]; then
  echo "ERROR. Empty ENVOY_PASSWORD variable."
  exit 1
fi

if [ -z "${ENVOY_IP}" ]; then
  echo "ERROR. Empty ENVOY_IP variable."
  exit 1
fi

if [ -z "${ENVOY_SITE}" ]; then
  echo "ERROR. Empty ENVOY_SITE variable."
  exit 1
fi

if [ -z "${ENVOY_SERIAL}" ]; then
  echo "ERROR. Empty ENVOY_SERIAL variable."
  exit 1
fi

openssl req -x509 -nodes -days 365 -newkey rsa:2048 -subj "/C=ES/ST=Barcelona/L=Sant Adria de Besos/O=oteixido.net/CN=envoy" -keyout /etc/ssl/private/selfsigned.key -out /etc/ssl/certs/selfsigned.crt

export ENVOY_TOKEN=$(envoy-create-token)
if [ "$?" == "0" ]; then
  envsubst '${ENVOY_TOKEN} ${ENVOY_IP}' < /etc/nginx/conf.d/default.conf | sponge /etc/nginx/conf.d/default.conf
else
  rm -f /etc/nginx/conf.d/*
fi