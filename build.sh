#!/bin/bash -e

VERSION=$1
if [ -z "$VERSION" ]; then
    VERSION=latest
fi

docker buildx build \
    --push \
    --platform linux/arm/v7,linux/arm64,linux/amd64 \
    --tag oteixido/envoy:${VERSION} \
    .
